Contributors' Guide

Best Practices

Please follow the following guidelines for managing the repository -

0. Create issues for tasks/bugs/features to be implemented.
   Assign them to yourself or to someone from the team.

1. Create a separate branch for each issue you're working on.

2. Keep the master branch clean. Do not merge any branch into the
   master until the code has been tested.

3. Send a pull request for your branch to be merged. It will
   reviewed by everyone in the team and then merged.

4. Before sending a PR, please make sure that you rebase on
   master/appropriate feature branch if it has progressed
   while you were developing.
   Please squash your commits into few logical ones for each
   changeset.

5. Try to write commit messages in present tense.

